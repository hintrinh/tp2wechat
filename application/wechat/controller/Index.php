<?php
namespace app\wechat\controller;

use app\wechat\model\Wechat;
use think\Controller;

class Index extends Controller
{
    public function index()
    {
        $echoStr = isset($_GET['echostr']) ? $_GET['echostr'] : '';
        $wechat = new Wechat();
        if ($wechat->checkSignature() && $echoStr) {
            return $echoStr;
        } else {
            $postObj = $wechat->sdkInit();
            if(strtolower($postObj->MsgType) == 'event' && strtolower($postObj->Event) == 'subscribe'){
                $this->checkFocus();
            }
            $this->checkClickEvent();
//            $wechat->responseGraphicMsg($postObj,$wechat->wechatHotSpider('农旅',0 , 'wechatAllHot'));
        }
    }

    public function createMenu()
    {
//        error_reporting(1|4);
        $wechat = new Wechat();
        $menuArr = array(
            array(
                'type' => 'view',
                'name' => urlencode('访问官网'),
                'url' => 'http://www.lanlw.cn'
            ),
            array(
                'type' => 'click',
                'name' => urlencode('每日速递'),
                'key' => 'lanl_submenu_4'
            ),
            array(
                'name' => urlencode('获取更多'),
                'sub_button' => array(
                    array(
                        'type' => 'click',
                        'name' => urlencode('精选视频'),
                        'key' => 'lanl_submenu_3'
                    ),
//                    array(
//                        'type' => 'click',
//                        'name' => urlencode('获取token'),
//                        'key' => 'lanl_submenu_2'
//                    ),
//                    array(
//                        'type' => 'click',
//                        'name' => urlencode('更新自定义菜单'),
//                        'key' => 'lanl_submenu_3'
//                    ),
                    array(
                        'type' => 'click',
                        'name' => urlencode('农旅热点'),
                        'key' => 'lanl_submenu_5'
                    )
                )
            )
        );

//        return $wechat->defineMenu('curl','access_token', $menuArr);
        $postArr = array(
            'button' => $menuArr
        );
        $postArr = urldecode(json_encode($postArr));
        $url = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token=".$wechat->getAccessToken('curl',
                'access_token');
        return $wechat->curlPost($url,$postArr);
    }

    public function checkClickEvent()
    {
        $wechat = new Wechat();
        $postObj = $wechat->sdkInit();
        $key = isset($postObj->EventKey) ? $postObj->EventKey : '';
        switch ($key) {
            case 'lanl_submenu_1':
                $this->createMenu();
                break;
            case 'lanl_submenu_2':
                $wechat->debug($wechat->getAccessToken('curl','access_token'));
                break;
            case 'lanl_submenu_3':
                $graphicArr = array(
                    array(
                        'title' => '韶关南华寺',
                        'description' => '中国佛教名寺之一，是禅宗六祖惠能宏扬“南宗禅法”的发源地',
                        'picurl' => 'http://i4.buimg.com/589225/a8837d9b64ddd846.jpg',
                        'url' => 'http://www.bilibili.com/video/av8810804/'
                    ),
                    array(
                        'title' => '汤溪水库',
                        'description' => '广东省东部的大型供水水库，集水面积667平方公里，总库容3.78亿立方米',
                        'picurl' => 'http://i4.buimg.com/589225/8597b78965a286a3.jpg',
                        'url' => 'http://m.youku.com/video/id_XMTQ4NTU5NTgxMg==.html'
                    ), array(
                        'title' => '韶关丹霞山',
                        'description' => '广东省面积最大的风景区、以丹霞地貌景观为主的风景区和世界自然遗产地',
                        'picurl' => 'http://i4.buimg.com/589225/4f31f00a7f4e1844.jpg',
                        'url' => 'http://www.tudou.com/programs/view/AfdfofchEuk/'
                    ),
                );
                break;
            case 'lanl_submenu_4' :
                $graphicArr = $wechat->wechatHotSpider('农旅', 1 , 'wechatHot');
                break;
            case 'lanl_submenu_5' :
                $graphicArr = $wechat->wechatHotSpider('农旅', 0 , 'wechatAllHot');
        }
        if(isset($graphicArr)){
            $postObj = $wechat->sdkInit();
            $wechat->responseGraphicMsg($postObj, $graphicArr);
        }
    }

    public function clear()
    {
        $wechat = new Wechat();
        $cache = $wechat->memcacheInit();
        $cache->clear('curl');
        return 'true';

    }

    public function checkFocus()
    {
        $wechat = new Wechat();
        $postObj = $wechat->sdkInit();
        $url = 'https://mp.weixin.qq.com/mp/profile_ext?action=home&__biz=MzUzOTAxODMzOA==&scene=115#wechat_redirect/';
        $content = '/鼓掌/鼓掌/鼓掌感谢亲的关注~~小编会竭诚为您呈现最新的农旅资讯与最及时的政策利好。

        中国农业旅游网依托海峡两岸农业发展研究院，面向两岸暨港澳、海外侨胞全体中华儿女，提供现代农业和旅游的政、资、产、学、研、用等最新资讯，主要在“农旅产业”下功夫，在“两岸交流”做文章，通过产业的交流与合作，促进两岸暨港澳对中华民族的凝聚力，为两岸和平发展贡献一份应尽的责任。

欢迎各位有识之士踊跃来稿<a href="mailto:admin@lanyy.net">admin@lanyy.net</a>/握手/握手/握手';
        $content1 = '<a href="https://mp.weixin.qq.com/mp/profile_ext?action=home&__biz=MzUzOTAxODMzOA==&scene=115
        #wechat_redirect">点击可查看历史消息</a>';
        $wechat->responseTextMsg($postObj, $content);
    }
    public function getToken(){
        $wechat = new Wechat();
        return $wechat->getAccessToken('curl','access_token');
    }
    public function wechatHot(){
        $wechat = new Wechat();
        halt($wechat->wechatHotSpider('农旅',1,'wechatHot'));
    }
}


