<?php
namespace app\wechat\controller;
use think\Controller;
use think\Exception;

class Test extends Controller {
    public function index(){
        $limitDay = 1;
        $keyword = $_GET['keyword'];
        $url = "http://weixin.sogou.com/weixin?type=2&ie=utf8&query=".$keyword."&ft=&et=&interation=null&wxid=&usip=null&from=tool";
        if($limitDay){
            $url .= "&tsn=".$limitDay;
        }
        $curlobj = curl_init();			// 初始化
        curl_setopt($curlobj, CURLOPT_URL, $url);		// 设置访问网页的URL
        curl_setopt($curlobj, CURLOPT_RETURNTRANSFER, true);			// 执行之后不直接打印出来
            curl_setopt($curlobj, CURLOPT_FOLLOWLOCATION,1);
        curl_setopt($curlobj, CURLOPT_TIMEOUT,300);
//        curl_setopt($curlobj, CURLOPT_HTTPHEADER, array('X-FORWARDED-FOR:183.36.114.45:80', 'CLIENT-IP:115.182.41.180'));
        curl_setopt($curlobj, CURLOPT_REFERER, "http://weixin.sogou.com/");
//        curl_setopt($curlobj, CURLOPT_PROXY, "http://58.222.254.11:3128");
        curl_setopt($curlobj, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:52.0) Gecko/20100101 Firefox/52.0");
        $output = curl_exec($curlobj);
//        echo $output;
        curl_close($curlobj);			// 关闭cURL
        include_once "../public/simple_html_dom.php";
        $dom = new \simple_html_dom();
        try{
            $dom->load($output);
            $newsList = $dom->find('ul[class=news-list]',0);
            dump($newsList);
            $temp = array();
            $count = count($newsList->children);
            for($i=1,$j=0;$i<$count;$i+=3,$j++){
                if($newsList->children($i)->children(0)->attr['class'] == 'img-box'){
                    $temp[$j]['url'] = html_entity_decode($newsList->children($i)->children(0)->children(0)->attr['href']);
                    $temp[$j]['title'] = trim( html_entity_decode($newsList->children($i)->children(1)->children(0)->plaintext));
                    $temp[$j]['summary'] = trim( html_entity_decode($newsList->children($i)->children(1)->children(1)->plaintext));
                    $temp[$j]['src'] = html_entity_decode($newsList->children(1)->children(0)->children(0)->children(0)->attr['src']);
                }else{
                    $j--;
                }
            }

        }catch (Exception $e){
        }
//        return $temp;
    }

}