<?php
return [
    'type' => 'mysql',
    'dsn' => '',
    'host' => '127.0.0.1',
    'database' => 'wechat',
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8',
    'prefix' => 'wechat_',
    'debug' => false,
    'deploy' => '0',
    'rw_separate' => false,
    'master_num' => 1,
    'slave_no' => '',
    'fields_strict'=> true
];