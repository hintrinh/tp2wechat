<?php
namespace app\query2ncp\controller;
use think\Controller;
use think\Cache;

include_once "../public/simple_html_dom.php";

/**
* 
*/
class Index extends Controller
{
    public function __construct(){
        parent::__construct();
        $this->cache = $this->memcacheInit();
        // $this->dom = 
        // $this->getProductType = $this->getProductType('ncp','getProductType');
        // $this->getProductList = $this->getProductList('ncp','getProductList');
    }

	public function index(){
		// $this->cache->clear();
        // error_reporting(E_ERROR | E_WARNING | E_PARSE);
        $url = "http://nc.mofcom.gov.cn/channel/jghq2017/index.shtml";
        $result = array();
        $content = mb_convert_encoding($this->curlByUrl($url),'UTF-8','GBK');
        $dom = new \simple_html_dom();;
         $dom->load($content);
         // while ($temp1=$dom->find('table[class=price-table]',$i]) {
         //    while($temp2 = $temp1)
         // }
        $i = 0;
        $j = 0;
        while($temp = $dom->find('table[class=price-table]',$i++)){
            while($temp = $temp->children($j++)){
                $result[$i][$j][] = trim($temp->children(0)->children(0)->plaintext);
                $result[$i][$j][] = trim($temp->children(1)->children(0)->plaintext);
                $result[$i][$j][] = trim($temp->children(2)->children(0)->plaintext);
                $result[$i][$j][] = trim($temp->children(3)->innertext);
            }      
        }
        $temp = $dom->find('table[class=price-table]',0);
        $temp = $temp->children(1);
        return trim($temp->children(1)->children(0)->plaintext);
        // $temp = $temp->find('tr',0);
        
        // return $temp->children(3)->innertext ;
        // $temp = $temp->children(4);
        // return $temp;
        // return dump($temp->children(0)->plaintext);
        // for($i=0;$i<=3;$i++){
        //     if($temp->children[$i]->children(0)->plaintext){
        //         $result[$i] = trim($temp->children[$i]->children(0)->plaintext);
        //     }
        //     $result[$i] = trim($temp);
        // }
        
        return dump($result);
		// $this->assign('getProductType',$this->getProductType);
  //       $this->assign('getProductList',$this->getProductList);
        // $getProductType = $this->getProductType;
        // return dump($getProductType);
        return $this->fetch();       
	}

    //获取所有类目的列表
    public function getBaseMarkList($tag = '' , $key = ''){
        $cache  = $this->cache;
        if (!$cache->has($key)) {
            $result = array();
            $url = "http://nc.mofcom.gov.cn/nc/js/data/get_base_mark_list.jsp";
            $content = mb_convert_encoding($this->curlByUrl($url),'UTF-8','GBK');
            $temp = mb_substr($content,38,9571,'GBK');
            // return $temp;
            $temp =str_replace(array('[',']','\''), '', $temp);
            $temp = explode(',',$temp);                    
            for($i = 0,$k = 0;$i<count($temp);$k++){
                for($j=0;$j<3;$j++){
                    $result[$k][$j] = $temp[$i++];                
                }            
            }
            $cache->tag($tag)->set($key, $result, 7200);
        }else{
            $result = $cache->get($key);
        }  
        return $result;
    }

    //获取产品的类型列表
    public function getProductType($tag = '' , $key = ''){        
        $cache  = $this->cache;
        if (!$cache->has($key)) {
            $result = array();
            $getBaseMarkList = $this->getBaseMarkList('ncp','getBaseMarkList'); 
            $i = 0;       
            foreach ($getBaseMarkList as $key => $value){
                if($getBaseMarkList[$key][0] == '0'){
                    $result[$i]['type'] = $value[1]; 
                    $result[$i]['id'] = $value[2]; 
                    $i++;
                }                            
            }
            $cache->tag($tag)->set($key, $result, 7200);
        }else{
            $result = $cache->get($key);
        }       
        return $result;
    }

    //获取产品列表
    public function getProductList($tag = '' , $key = ''){
        $cache = $this->cache;
        if(!$cache->has($key)){
            $result = array();
            $getBaseMarkList = $this->getBaseMarkList('ncp','getBaseMarkList');
            $getProductType = $this->getProductType('ncp','getProductType');
            foreach ($getProductType as $key1 => $value1) {
                foreach ($getBaseMarkList as $key2 => $value2) {
                    if($value2[0] == $value1['id']){
                        $result[$value1['id']][] = $value2[1];
                    }
                }
            }
            $cache->tag($tag)->set($key , $result , 7200);
        }else{
            $result = $cache->get($key);
        }
        return $result;
    }

    public function getNewestPrice($tag = '', $key = ''){

    }

    //获取入口链接，Step1
    // public function getEntranceLinks($tag = '' , $key = ''){
    //     $cache  = $this->cache;
    //     if (!$cache->has($key)) {           
    //         $url = "http://nong.gold600.com/";
    //         $content = $this->curlByUrl($url);
    //         $dom = new \simple_html_dom();
    //         $dom->load($content);
    //         $newsList = $dom->find('div[class=border1]',0);
    //         $arr = array();
    //         $handle = $newsList->first_child();
    //         $arr['title'] = trim($handle->innertext);
    //         $i = -1;
    //         $j = 0;
    //         while($handle->next_sibling()){
    //             $handle = $handle->next_sibling();
    //             if($handle->tag == 'b'){
    //                 $i++;
    //                 $j = 0;
    //                 $arr[$i]['label'] = $handle->innertext;
    //             }elseif($handle->tag == 'a'){
    //                 $arr[$i]['data'][$j]['url'] = trim($handle->attr['href']);
    //                 $arr[$i]['data'][$j]['text'] = trim($handle->innertext);
    //                 $j++;
    //             }                
    //         }
    //         $cache->tag($tag)->set($key, $arr, 7200);
    //     }else{
    //         $arr = $cache->get($key);
    //     }
    //     return $arr;
    // }

    //通过curl抓取url内容并返回
    public function curlByUrl($url){
        header("Content-Type:text/html;charset=utf-8");
        $curl = curl_init();
        $ip_long = array(
                array('607649792', '608174079'), //36.56.0.0-36.63.255.255
                array('1038614528', '1039007743'), //61.232.0.0-61.237.255.255
                array('1783627776', '1784676351'), //106.80.0.0-106.95.255.255
                array('2035023872', '2035154943'), //121.76.0.0-121.77.255.255
                array('2078801920', '2079064063'), //123.232.0.0-123.235.255.255
                array('-1950089216', '-1948778497'), //139.196.0.0-139.215.255.255
                array('-1425539072', '-1425014785'), //171.8.0.0-171.15.255.255
                array('-1236271104', '-1235419137'), //182.80.0.0-182.92.255.255
                array('-770113536', '-768606209'), //210.25.0.0-210.47.255.255
                array('-569376768', '-564133889'), //222.16.0.0-222.95.255.255
            );
        $rand_key = mt_rand(0, 9);
        $ip= long2ip(mt_rand($ip_long[$rand_key][0], $ip_long[$rand_key][1]));
        $options = array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => 1 ,
            CURLOPT_HTTPHEADER => array('X-FORWARDED-FOR:183.36.114.45:80', 'CLIENT-IP:'.$ip),
            CURLOPT_REFERER => $url,
            CURLOPT_USERAGENT => "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:52.0) Gecko/20100101 Firefox/52.0"

        );
        curl_setopt_array($curl, $options);
        $content = curl_exec($curl);
        curl_close($curl);
        return $content;
    }

    public function memcacheInit()
    {
        $option = array(
            'type' => 'memcache',
            'host' => '127.0.0.1',
            'port' => '11211',
            'timeout' => 10,
            'expire' => 7000,
            'prefix' => 'ncp_',
        );
        $cache = Cache::connect($option);
        return $cache;
    }
	
}
